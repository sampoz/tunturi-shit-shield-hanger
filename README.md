# Tunturi shit shield hanger
![Shit shield hanger in place](hanger_in_place.jpg)

My trusty tunturi rx-700 lost it's loskasuoja(shit-shield) fixture, so I modeled
a replacement part with solvespace.

Part is currently in my bike, printed with prusa i3 mark3 and PETG.

# More pictures

![A view from side of the bike](sideview.jpg)

![Comparison to original part](compared_original.jpg)
